# Barcode reader
This project can identify most bar codes based on a byte pdf file,
consumer accounts and QR codes were tested. For the reading to work, you must send an api
byte pdf converted to base 64.



**Install:**

```
docker build -t barcode .
```

**Execute:**

```
docker run -p 5000:5000 barcode
```


To read the file converted to base64 send an api post with the key **pdf_base64**. 

```
POST http://0.0.0.0:5000/v1/barcode-reader/

{
    pdf_base64: JVBERi0xLjMKJcTl8...
}
```


Return of json with barcode reader.
```
{
    "result": [
        {
            "data": "03394561400000178329632964000000000012520102",
            "type": "I25"
        }
    ]
}
```


This example of the value that should be sent to api, copy this base64 for testing. 

```
GET http://0.0.0.0:5000/v1/barcode-reader/exemple
```

```
{
    "base64": "JVBERi0xLjMKJcTl8uXrp_Og0MTGC...."
    
}
```

