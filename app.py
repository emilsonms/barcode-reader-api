from flask import Flask
from flask_restful import Api
from views.barcode import BarcodeReader, BarcodeReaderExemple

app = Flask(__name__)
api = Api(app)
api.add_resource(BarcodeReader, "/v1/barcode-reader/")
api.add_resource(BarcodeReaderExemple, "/v1/barcode-reader/exemple")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5000')