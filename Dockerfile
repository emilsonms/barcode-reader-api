FROM python:3.7
COPY . /barcode-reader-api
WORKDIR /barcode-reader-api
RUN apt-get update && apt-get install -y libzbar-dev libzbar0 && apt-get install -y poppler-utils
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]


