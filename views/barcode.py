from flask import request, jsonify
from flask_restful import Resource
from pdf2image import convert_from_bytes
import pyzbar.pyzbar as pyzbar
import cv2
import base64
import os



class BarcodeReader(Resource):

    def post(self):
        try:
            file = request.form.get("pdf_base64", None)

            if file is None:
                return jsonify({"error": "Empty file"})

            file = base64.b64decode(file, altchars="-_")
            png = convert_from_bytes(file)

            i = 0
            for page in png:
                page.save('output/file{}.jpg'.format(i), 'JPEG')
                i = i + 1

            for j in range(0, i):
                im = cv2.imread('output/file{}.jpg'.format(j))
                decoded = self.decode(im)

            for f in range(0, i):
                tmp = 'output/file{}.jpg'.format(f)
                if os.path.exists(tmp):
                    os.remove(tmp)

            result = []

            for obj in decoded:
                result.append({"type": obj.type, "data": obj.data.decode()})

            return jsonify({"result": result})

        except Exception as e:
            return jsonify({"error": str(e)})


    def decode(self, im):
        decoded = pyzbar.decode(im)

        for obj in decoded:
            print('Type : ', obj.type)
            print('Data : ', obj.data, '\n')

        return decoded



class BarcodeReaderExemple(Resource):

    def get(self):
        try:
            file = open("exemple_base64/base64.txt")
            return jsonify({"base64": file.read()})

        except Exception as e:
            return jsonify({"error": str(e)})
